#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal.h>

#define ONE_WIRE_BUS 7
#define TEMPERATURE_PRECISION 12
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress tempDeviceAddress;

float tempC;
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Tableau Pin de LED
int pins[] = {6, 8, 9, 10};
int tot_pins = 4;
void loopLED() {
  //vers l'infini
  for (int nb_pin = 0; nb_pin <= tot_pins; nb_pin++) {
    digitalWrite(pins[nb_pin], HIGH);
    delay(100);

  }
  for (int nb_pin = 0; nb_pin <= tot_pins; nb_pin++) {
    digitalWrite(pins[nb_pin], LOW);
    delay(100);
    //et au dela
  }
  delay(100);
  for (int nb_pin = tot_pins; nb_pin >= 0; nb_pin--) {
    digitalWrite(pins[nb_pin], HIGH);
    delay(100);

  }
  for (int nb_pin = tot_pins; nb_pin >= 0; nb_pin--) {
    digitalWrite(pins[nb_pin], LOW);
    delay(100);

  }
}

void setup(void) {
  // opening serial port :
  Serial.begin(9600);
  sensors.begin();
  int numberOfDevices = sensors.getDeviceCount();

  sensors.getAddress(tempDeviceAddress, 0);
  tempC = sensors.getTempC(tempDeviceAddress);
  Serial.println(tempC);
  //"alimentation" des LED
  for (int nb_pin = 0; nb_pin < tot_pins; nb_pin++) {
    pinMode(pins[nb_pin], OUTPUT);
    delay(1000);
  }

  //text top line
  lcd.begin(16, 2);
  lcd.print("Is it Hot ?");

}

void loop() {
  int numberOfDevices = sensors.getDeviceCount(); {

    // Send the command to get temperatures
    sensors.requestTemperatures();
    float tC = sensors.getTempC(tempDeviceAddress);
    Serial.println(tC);

    // second line du LCD
    lcd.setCursor(0, 1);
    lcd.print("Temp:");
    lcd.print(tC);
    lcd.print("C"); delay(2000);
  }
  loopLED();

}
